;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Name: Anthony Do
; Date: 02/18/2014
; Class: CS 3210
; Assignment: LISP Assignment 2
;
; Notes: after talking to a few students, they recommended that i
; 		 shouldnt re-invent the wheel.  Seeing how some of the
; 		 functions we did for the first assignment can be utilized
; 		 here.  So i will try....
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(load "functions")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Problem 1
; 
; returns the sum of all the days for every given task in the list X
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun sum (X)
	(apply 'sumAll (getIndexAt X 1))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Problem 2
; 
; returns all the parent tasks of the given task in List X
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun predecessors (task X)
	(cond
		((null task) nil)
		((trueTask task X) (cddr (car X)))
		((predecessors task (cdr X)))
	)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Problem 3
; 
; Returns the time for a given task in list X
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun getTime (task X)
	(cond
		((trueTask task X) (getIndexAt(car X) 1))
		((getTime task (cdr X)))
	)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;  Helper functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun getIndexAt (X i)
	(cond
		((NULL X) nil)
		((= i 0) car X)
		((getIndexAt(cdr X)(- i 1)))
	)
)

(defun trueTask (task X)
	(equal task (caar X))
)